import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

// Send to outSide this optionSelected property set to Output
@Output() optionSelected = new EventEmitter<string>();
  constructor() { }

  ngOnInit() {
  }

  onSelect(option: string){
    console.log(option);

    this.optionSelected.emit(option);
    // console.log(this.optionSelected);
  }

}
