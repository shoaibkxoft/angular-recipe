import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../shared/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  recipes: Recipe[] = [
    new Recipe('Steak', 'Using this recipe you can create a Steak',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQAzQP9gneQsGKAc3xSDsvEsnlj9UQ0Hz8hn_GHwdh6RCyo5jC2'),
    new Recipe('Rib Eye', 'Using this recipe you can create a Rib Eye',
               'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg')
  ];

  constructor() { }

  ngOnInit() {
  }

}
