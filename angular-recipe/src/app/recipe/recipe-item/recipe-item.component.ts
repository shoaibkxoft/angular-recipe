import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from 'src/app/shared/recipe.model';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {

  //Get Recipe from outSide so make @Input to get it 
  @Input() recipe: Recipe;
  constructor() { }

  ngOnInit() {
  }

}
